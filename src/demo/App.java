package demo;

import demo.models.Address;
import demo.models.CanResearch;
import demo.models.CanTeach;
import demo.models.Person;
import demo.models.PhD;
import demo.models.Researcher;
import demo.models.Student;
import demo.models.Teacher;

public class App {
	
	public static void main(String[] args) {
		
		Address a = new Address("Paris");

		Person[] persons = {
				new Teacher("andre", 39, new Address[]{ a }, 10000) , 
				new Student("lucie", 10, new Address[]{ new Address("Marseille"), new Address("Toulouse") }, 10), 
				new Student("pierre", 6, new Address[]{ new Address("Bordeaux") }, 12) };
		
		for (Person p : persons) {
			System.out.println(p);
			p.sayHello();
		}
		
		
		CanTeach [] teachers = {
			new Teacher("andre1", 39, new Address[]{ a }, 10000),
			new PhD("andre2", 39, new Address[]{ a })
		};
		for (CanTeach t : teachers)
			t.teach();

		
		CanResearch [] researchers = {
			new Researcher("andre1", 39, new Address[]{ a }),
			new PhD("andre2", 39, new Address[]{ a })
		};
		for (CanResearch r : researchers) {
			r.research();
		}
		
		System.out.println(Person.getPersonNb());
		
		
		
		
		
		
	}

}
