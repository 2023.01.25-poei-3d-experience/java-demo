package demo.models;

public class PhD extends Person implements CanTeach, CanResearch {

	public PhD(String name, int age, Address[] addresses) {
		super(name, age, addresses);
	}

	@Override
	public void research() {
		System.out.println("researching (PhD)");
	}

	@Override
	public void sayHello() {
		System.out.println("Hello PhD !");
	}
	
}
