package demo.models;

public interface CanResearch {
	
	void research();
	
}
