package demo.models;

public class Teacher extends Person implements CanTeach {

	private double salary;
	
	public Teacher(String name, int age, Address [] addresses, double s) {
		super(name, age, addresses);
		this.salary = s;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Teacher [salary=" + salary + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void sayHello() {
		System.out.println("Hello teacher !");
	}

	public void teach() {
		System.out.println("teaching (teacher)");
	}
	
}
