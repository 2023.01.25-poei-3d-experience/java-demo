package demo.models;

public interface CanTeach {

	default void teach() {
		System.out.println("teaching (default)");
	};
	
}
