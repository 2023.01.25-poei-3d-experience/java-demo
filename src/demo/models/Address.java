package demo.models;

public class Address {

	private String city;
	
	private Person owner;

	public Address(String city) {
		super();
		this.city = city;
	}

	public Address(String city, Person owner) {
		super();
		this.city = city;
		this.owner = owner;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Address [city=" + city + ", owner=" + owner.getName() + "]";
	}
	
}
