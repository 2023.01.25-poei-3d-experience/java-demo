package demo.models;

public class Researcher extends Person implements CanResearch {


	public Researcher(String name, int age, Address[] addresses) {
		super(name, age, addresses);
	}

	@Override
	public void research() {
		System.out.println("researching (researcher)");
	}

	@Override
	public void sayHello() {
		System.out.println("Hello researcher !");
	}

}
