package demo.models;

public class Student extends Person {

	private double score;
	
	public Student(String name, int age, Address [] addresses, double score) {
		super(name, age, addresses);
		this.score = score;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Student [score=" + score + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void sayHello() {
		System.out.println("Hello student !");
	}
	
}
