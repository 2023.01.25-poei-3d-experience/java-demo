package demo.models;

public abstract class Person {

	private String name;
	
	private int age;
	
	private Address [] addresses;
	
	private static int personNb = 0;
	
	public Person(String name, int age, Address [] addresses) {
		super();
		this.name = name;
		this.age = age;
		this.addresses = addresses;
		for (Address a : addresses)
			a.setOwner(this);
		personNb++;
	}

	public abstract void sayHello();

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Address [] getAddresses() {
		return addresses;
	}

	public void setAddresses(Address [] addresses) {
		this.addresses = addresses;
	}

	public static int getPersonNb() {
		return personNb;
	}
	
	@Override
	public String toString() {
		String addressesString = "";
		for (Address a : addresses) {
			addressesString += a + ", ";
//			addressesString += a.toString() + (a != addresses[addresses.length - 1] ? ", " : "");
		}
		return "Person [name=" + name + ", age=" + age + ", address=[" + addressesString + "]]";
	}
	
}
