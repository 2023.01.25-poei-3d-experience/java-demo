package demo.exceptions;

import java.util.Scanner;

public class App {
	
	public static int getAge(Scanner scanner) throws AgeInvalideException {
		System.out.println("age ?");
		int age;
		try {
			age = Integer.parseInt(scanner.nextLine());
		} catch (NumberFormatException e) {
			throw new AgeInvalideException("conversion en int echouée", e);
		}
		if (age < 0)
			throw new AgeInvalideException("age ne peut etre negatif");
		return age;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int age = 0;
		boolean valid;
		do {
			valid = true;

			try {
				age = getAge(scanner);
			} catch (AgeInvalideException e) {
				System.out.println("age invalide.");
				valid = false;	
			}
		} while (!valid);
		System.out.println("Vous etes " + ((age < 18) ? "mineur" : "majeur"));
		scanner.close();
	}

}
