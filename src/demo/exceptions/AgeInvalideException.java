package demo.exceptions;

public class AgeInvalideException extends Exception {

	public AgeInvalideException() {
		super();
	}

	public AgeInvalideException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AgeInvalideException(String message, Throwable cause) {
		super(message, cause);
	}

	public AgeInvalideException(String message) {
		super(message);
	}

	public AgeInvalideException(Throwable cause) {
		super(cause);
	}

}
