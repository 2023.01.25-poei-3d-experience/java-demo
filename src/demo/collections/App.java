package demo.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class App {

	public static void main(String[] args) {
		
		List<Person> peopleList = new ArrayList<>();
		peopleList.add(new Person("andre", 39));
		peopleList.add(new Person("lucie", 10));
		peopleList.add(new Person("pierre", 6));
		peopleList.add(new Person("andre", 39));
		
		System.out.println("> List (for avec compteur - PAS OPTIMAL)");
		for (int i=0; i<peopleList.size(); i++)
			System.out.println("\t" + peopleList.get(i));
		
		System.out.println("> List (avec iterateur - OPTIMAL MAIS MOCHE)");
		Iterator<Person> iterator = peopleList.iterator();
		while (iterator.hasNext()) {
			Person p = iterator.next();
			System.out.println("\t" + p);
		}
		
		System.out.println("> List (foreach - OPTIMAL ET JOLI)");
		for (Person p : peopleList)
			System.out.println("\t" + p);
		

		System.out.println("> HashSet - pas de doublon (equals & hashcode), pas d'accès par indice et ne préserve pas l'ordre d'insertion");
		Set<Person> peopleHashSet = new HashSet<>();
		peopleHashSet.add(new Person("andre", 39));
		peopleHashSet.add(new Person("lucie", 10));
		peopleHashSet.add(new Person("pierre", 6));
		peopleHashSet.add(new Person("andre", 39));
		peopleHashSet.add(new Person("marin", 6));
		peopleHashSet.add(new Person("lucie", 12));
		for (Person p : peopleHashSet)
			System.out.println("\t" + p + " " + p.hashCode());
		

		System.out.println("> LinkedHashSet - pas de doublon (equals & hashcode), pas d'accès par indice et préserve l'ordre d'insertion");
		Set<Person> peopleLinkedHashSet = new LinkedHashSet<>();
		peopleLinkedHashSet.add(new Person("andre", 39));
		peopleLinkedHashSet.add(new Person("lucie", 10));
		peopleLinkedHashSet.add(new Person("pierre", 6));
		peopleLinkedHashSet.add(new Person("andre", 39));
		peopleLinkedHashSet.add(new Person("marin", 6));
		peopleLinkedHashSet.add(new Person("lucie", 12));
		for (Person p : peopleLinkedHashSet)
			System.out.println("\t" + p);

		
		System.out.println("> TreeSet with natural order - pas de doublon (relation d'ordre), pas d'accès par indice et maintient la collection triée");
		Set<Person> peopleTreeSet = new TreeSet<>();
		peopleTreeSet.add(new Person("andre", 39));
		peopleTreeSet.add(new Person("lucie", 10));
		peopleTreeSet.add(new Person("pierre", 6));
		peopleTreeSet.add(new Person("andre", 39));
		peopleTreeSet.add(new Person("marin", 6));
		peopleTreeSet.add(new Person("lucie", 12));
		for (Person p : peopleTreeSet)
			System.out.println("\t" + p);

		
		System.out.println("> TreeSet with explicit order - pas de doublon (relation d'ordre), pas d'accès par indice et maintient la collection triée");
		peopleTreeSet = new TreeSet<>(new PersonComparatorByAgeAscending());
		peopleTreeSet.add(new Person("andre", 39));
		peopleTreeSet.add(new Person("lucie", 10));
		peopleTreeSet.add(new Person("pierre", 6));
		peopleTreeSet.add(new Person("andre", 39));
		peopleTreeSet.add(new Person("marin", 6));
		peopleTreeSet.add(new Person("lucie", 12));
		for (Person p : peopleTreeSet)
			System.out.println("\t" + p);
		
		
		System.out.println("> Map");
		Map<String, Person> peopleMap = new HashMap<>();
		peopleMap.put("andre", new Person("andre", 39));
		peopleMap.put("lucie", new Person("lucie", 10));
		peopleMap.put("pierre", new Person("pierre", 6));
		
		System.out.println("   - accès par clé à un élément");
		System.out.println("\t" + peopleMap.get("lucie"));
		
		System.out.println("   - parcours avec keySet()");
		for (String key : peopleMap.keySet())
			System.out.println("\t" + key + " => " + peopleMap.get(key));
		
		System.out.println("   - parcours avec values()");
		for (Person value : peopleMap.values())
			System.out.println("\t" + value);
		
		System.out.println("   - parcours avec entrySet()");
		for (Entry<String, Person> entry : peopleMap.entrySet())
			System.out.println("\t" + entry.getKey() + " => " + entry.getValue());
		
	}

}
