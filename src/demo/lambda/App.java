package demo.lambda;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class App {

	public static interface InterfaceNonFonctionelle {
		void methodeAbtraite1();

		void methodeAbtraite2();

		default void methodeNonAbstraite1() {
			System.out.println("j'ai une implémentation");
		}

		default void methodeNonAbstraite2() {
			System.out.println("j'ai une implémentation");
		}
	}

	@FunctionalInterface
	public static interface InterfaceFonctionelle {
		void methodeAbtraite();

		default void methodeNonAbstraite1() {
			System.out.println("j'ai une implémentation");
		}

		default void methodeNonAbstraite2() {
			System.out.println("j'ai une implémentation");
		}
	}

	public static class ClasseImplementantUneInterfaceNonFonctionelle implements InterfaceNonFonctionelle {
		@Override
		public void methodeAbtraite1() {System.out.println("j'ai une implémentation");}
		@Override
		public void methodeAbtraite2() {System.out.println("j'ai une implémentation");}
	}

	public static class ClasseImplementantUneInterfaceFonctionelle implements InterfaceFonctionelle {
		@Override
		public void methodeAbtraite() {System.out.println("j'ai une implémentation");}
	}
	
	public static void methodeAyantLaMemeSignatureQueLaMethodeAbstraiteDeLinterfaceFonctionelle() {
		System.out.println("j'ai une implémentation");
	}

	public static void main(String[] args) {
		// The 4 ways of instantiating functional interface :
		InterfaceFonctionelle interfaceFonctionelle;
		InterfaceNonFonctionelle interfaceNonFonctionelle;
		// 1. with an instance of a class implementing the functional interface
		interfaceFonctionelle = new ClasseImplementantUneInterfaceFonctionelle();
		interfaceNonFonctionelle = new ClasseImplementantUneInterfaceNonFonctionelle();
		// 2. with an anonymous class
		interfaceFonctionelle = new InterfaceFonctionelle() {
			@Override
			public void methodeAbtraite() {System.out.println("j'ai une implémentation");}
		};
		interfaceNonFonctionelle = new InterfaceNonFonctionelle() {
			@Override
			public void methodeAbtraite1() {System.out.println("j'ai une implémentation");}
			@Override
			public void methodeAbtraite2() {System.out.println("j'ai une implémentation");}
		};
		// 3. with a lambda function
		interfaceFonctionelle = () ->  System.out.println("j'ai une implémentation");
		// 4. with a method reference
		interfaceFonctionelle = App::methodeAyantLaMemeSignatureQueLaMethodeAbstraiteDeLinterfaceFonctionelle;
		
		// Predefined functional interface
		// Function<T,R> => R apply(T)
		Function<String, Integer> function = (str) -> { return str.length(); };
		function.apply("andre");
		// BiFunction<T1,T2,R> => R apply(T1, T2)
		BiFunction<String, String, Integer> bifunction = (str1, str2) -> { return str1.length() + str2.length(); };
		bifunction.apply("andre", "abrame");
		// Consumer<T> => void accept(T)
		Consumer<String> consumer = (str) -> { System.out.println(str); };
		consumer.accept("andre");
		// Supplier<R> => R get()
		Supplier<String> supplier = () -> { return "andre"; };
		supplier.get();
		// Predicate<T> => boolean test(T)
		boolean even = false;
		Predicate<String> predicate = (str) -> { return str.length() %2 == 0; };
		boolean result = predicate.test("andre");
		// Et plein d'autres :
		//  - UnaryOperator, BinaryOperator
		//  - DoubleFunction, DoubleConsumer, DoubleBinaryOperator, DoublePredicate, DoubleSupplier
		//  - LongFunction, LongConsumer, LongBinaryOperator, LongPredicate, LongSupplier...
	
	}
	
	

}
